﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Extends;

namespace SpaceInvadersArmory
{
    public class WeaponImporter
    {
        public static Dictionary<string, int> ReadFromText(string path, int minimumLength, List<string> blacklist)
        {
            Dictionary<string, int> keyValuePairs = new Dictionary<string, int>();
            try
            {
                string[] arrayString = File.ReadAllText(path).Split(' ');
                foreach (string word in arrayString) {
                    string wordTypo = word.Proper().Replace("\\p{P}+", "");
                    if (wordTypo.Length >= minimumLength && !blacklist.Contains(wordTypo))
                    {
                        if (keyValuePairs.ContainsKey(wordTypo)) keyValuePairs[wordTypo]++;
                        else keyValuePairs.Add(wordTypo, 1);
                    }
                }
            } catch (Exception)
            {
                throw new Exception("Le Fichier n'existe pas.");
            }
            return keyValuePairs;
        }

        public static void ImportWeaponsConsole(string path)
        {
            int minimumLength;
            string tempBL = "";
            List<string> blacklist = new List<string>();
            Console.WriteLine("Longueur minimale du mot : ");

            while(!int.TryParse(Console.ReadLine(), out minimumLength))
            {
                Console.WriteLine("Nombre invalide, Réessayer");
            }
            Console.WriteLine("Nom en liste noire, mettez deux fois le même mot pour terminer l'ajout : ");
            while(!blacklist.Contains(tempBL))
            {
                tempBL = Console.ReadLine();
                blacklist.Add(tempBL.ToLower().Replace("\\p{P}+", ""));
            }
            Dictionary<string, int> weaponKeys = ReadFromText(path, minimumLength, blacklist);
            foreach(KeyValuePair<string, int> weaponEntry in weaponKeys)
            {
                int minimumDamage, maximumDamage;
                if(weaponEntry.Key.Length > weaponEntry.Value)
                {
                    minimumDamage = weaponEntry.Value;
                    maximumDamage = weaponEntry.Key.Length;
                } else
                {
                    minimumDamage = weaponEntry.Key.Length;
                    maximumDamage = weaponEntry.Value;
                }
                Array enumValues = Enum.GetValues(typeof(EWeaponType));
                Random random = new Random();
                Armory.CreatBlueprint(weaponEntry.Key, (EWeaponType)enumValues.GetValue(random.Next(enumValues.Length)), minimumDamage, maximumDamage);
            }
        }

        public static bool ImportWeaponsFromFile(string path, int minimumLength = 3)
        {
            Dictionary<string, int> weaponKeys = ReadFromText(path, minimumLength, new List<string>());
            foreach (KeyValuePair<string, int> weaponEntry in weaponKeys)
            {
                int minimumDamage, maximumDamage;
                if (weaponEntry.Key.Length > weaponEntry.Value)
                {
                    minimumDamage = weaponEntry.Value;
                    maximumDamage = weaponEntry.Key.Length;
                }
                else
                {
                    minimumDamage = weaponEntry.Key.Length;
                    maximumDamage = weaponEntry.Value;
                }
                Array enumValues = Enum.GetValues(typeof(EWeaponType));
                Random random = new Random();
                Armory.CreatBlueprint(weaponEntry.Key, (EWeaponType)enumValues.GetValue(random.Next(enumValues.Length)), minimumDamage, maximumDamage);
            }
            return true;
        }
    }
}
