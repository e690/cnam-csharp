﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TPIHM.ViewModel;

namespace TPIHM.View
{
    /// <summary>
    /// Logique d'interaction pour CreatePlayerView.xaml
    /// </summary>
    public partial class CreatePlayerView : Window
    {
        public CreatePlayerViewModel ViewModel => DataContext as CreatePlayerViewModel;

        public CreatePlayerView()
        {
            InitializeComponent();
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ValidateBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel.FirstName != "" || ViewModel.LastName != "" || ViewModel.Alias != "")
                this.DialogResult = true;
            else
                MessageBox.Show("Veuillez saisir tous les informations du joueur !", "Erreur", MessageBoxButton.OK, MessageBoxImage.Warning);
        }
    }
}
