﻿using Microsoft.Win32;
using Models;
using SpaceInvadersArmory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TPIHM.ViewModel;

namespace TPIHM.View
{
    /// <summary>
    /// Logique d'interaction pour SpaceInvadersView.xaml
    /// </summary>
    public partial class SpaceInvadersView : Window
    {
        public SpaceInvadersViewModel ViewModel => DataContext as SpaceInvadersViewModel;
        public SpaceInvadersView()
        {
            InitializeComponent();
        }

        private void AddPlayerBtn_Click(object sender, RoutedEventArgs e)
        {
            CreatePlayerView createPlayerView = new CreatePlayerView();
            if ((bool)createPlayerView.ShowDialog())
            {
                ViewModel.Players.Add(new Player(createPlayerView.ViewModel.FirstName, createPlayerView.ViewModel.LastName, createPlayerView.ViewModel.Alias));
            }
        }

        private void AddWeaponBtn_Click(object sender, RoutedEventArgs e)
        {
            AddWeaponView addWeaponView = new AddWeaponView();
            if((bool)addWeaponView.ShowDialog())
            {
                try
                {
                    ViewModel.SelectedPlayer.BattleShip.AddWeapon(Armory.CreatWeapon(addWeaponView.ViewModel.SelectedWeaponBlueprint));
                    ViewModel.SpaceShipWeapons.ResetBindings();
                } catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void shipImageBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png",
                Title = "Select a picture"
            };
            if ((bool)dialog.ShowDialog())
            {
                BitmapImage image = new BitmapImage(new Uri(dialog.FileName));
                ViewModel.SelectedPlayer.BattleShip.Image = image;
                ViewModel.SpaceShipImage = image;
            }
        }

        private void importBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "Texte Files (*.txt)|*.txt|All files (*.*)|*.*"
            };
            if ((bool)dialog.ShowDialog())
            {
                try
                {
                    WeaponImporter.ImportWeaponsFromFile(dialog.FileName);
                    MessageBox.Show("Import réussi.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Impossible d'importer les données du fichier : {ex.Message}");
                }
            }
        }
    }
}
