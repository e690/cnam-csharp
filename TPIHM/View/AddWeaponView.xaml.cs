﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SpaceInvadersArmory;
using TPIHM.ViewModel;

namespace TPIHM.View
{
    /// <summary>
    /// Logique d'interaction pour AddWeaponView.xaml
    /// </summary>
    public partial class AddWeaponView : Window
    {
        public AddWeaponViewModel ViewModel => DataContext as AddWeaponViewModel;

        public AddWeaponView()
        {
            InitializeComponent();
        }

        private void ValidateBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel.SelectedWeaponBlueprint != null)
            {
                this.DialogResult = true;
            } else
            {
                MessageBox.Show("Veuillez choisir une Arme", "Erreur", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
