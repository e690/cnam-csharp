﻿using Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPIHM.Command
{
    public class RemovePlayerFromBindingList : CommandBase
    {
        public override void Execute(object parameter)
        {
            if (parameter != null)
            {
                object[] parameterArray = parameter as object[];
                ((BindingList<Player>)parameterArray[0]).Remove((Player)parameterArray[1]);
            }
        }
    }
}
