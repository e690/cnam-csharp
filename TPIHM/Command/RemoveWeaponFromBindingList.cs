﻿using Models.SpaceShips;
using SpaceInvadersArmory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace TPIHM.Command
{
    internal class RemoveWeaponFromBindingList : CommandBase
    {
        public override void Execute(object parameter)
        {
            if (parameter != null)
            {
                object[] parameterArray = parameter as object[];
                ((Spaceship)parameterArray[0]).RemoveWeapon((Weapon)parameterArray[2]);
                ((BindingList<Weapon>)parameterArray[1]).Remove((Weapon)parameterArray[2]);
                ((BindingList<Weapon>)parameterArray[1]).ResetBindings();
            }
        }
    }
}
