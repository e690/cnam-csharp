﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceInvadersArmory;

namespace TPIHM.ViewModel
{
    public class AddWeaponViewModel : ViewModelBase
    {
        private WeaponBlueprint _weaponBlueprint;
        public List<WeaponBlueprint> WeaponBluePrints => Armory.Blueprints;
        public WeaponBlueprint SelectedWeaponBlueprint { get => _weaponBlueprint; set => SetProperty(ref _weaponBlueprint, value); }
    }
}
