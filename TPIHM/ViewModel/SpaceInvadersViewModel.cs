﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Models;
using TPIHM.Command;
using SpaceInvadersArmory;
using Models.SpaceShips;
using System.Windows.Media.Imaging;

namespace TPIHM.ViewModel
{
    public class SpaceInvadersViewModel : ViewModelBase
    {
        private Player _selectedPlayer = null;
        private BindingList<Player> _players = new BindingList<Player>{ new Player("Yoann", "Bertuol", "Exda") };
        private BindingList<Weapon> _weapons = new BindingList<Weapon>();
        private BitmapImage _weaponImage = new BitmapImage();
        private Weapon _selectedWeapon = null; 

        public BindingList<Player> Players { get => _players;
            set
            {
                SetProperty(ref _players, value);
            }
        }
        public Player SelectedPlayer { get => _selectedPlayer;
            set
            {
                SetProperty(ref _selectedPlayer, value);
                SpaceShipWeapons = (value == null) ? new BindingList<Weapon>() : new BindingList<Weapon>(value?.BattleShip.Weapons);
                SpaceShipImage = (value == null) ? new BitmapImage() : value.BattleShip.Image;
            }
        }
        public Weapon SelectedWeapon { get => _selectedWeapon; set => SetProperty(ref _selectedWeapon, value); }

        public string PlayerName => (_selectedPlayer == null) ? "" : _selectedPlayer.Name;
        public string SpaceShipName => (_selectedPlayer == null) ? "" : _selectedPlayer.BattleShip.Name;
        public double SpaceShipStructure => (_selectedPlayer == null) ? 0 : _selectedPlayer.BattleShip.Structure;
        public double SpaceShipShield => (_selectedPlayer == null) ? 0 : _selectedPlayer.BattleShip.Shield;
        public double SpaceShipAverageDamage => (_selectedPlayer == null) ? 0 : _selectedPlayer.BattleShip.AverageDamages;
        public BindingList<Weapon> SpaceShipWeapons { get => _weapons; set => SetProperty(ref _weapons, value); }
        public BitmapImage SpaceShipImage { get => _weaponImage; set => SetProperty(ref _weaponImage, value); }

        public ICommand RemovePlayerFromBindingList { get; }
        public ICommand RemoveWeaponFromBindingList { get; }

        public SpaceInvadersViewModel()
        {
            RemovePlayerFromBindingList = new RemovePlayerFromBindingList();
            RemoveWeaponFromBindingList = new RemoveWeaponFromBindingList();
        }
    }
}
