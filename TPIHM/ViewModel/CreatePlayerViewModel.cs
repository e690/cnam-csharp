﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TPIHM.Command;

namespace TPIHM.ViewModel
{
    public class CreatePlayerViewModel : ViewModelBase
    {
        private string _firstName;
        private string _lastName;
        private string _alias;

        public string FirstName { get => _firstName; set => SetProperty(ref _firstName, value); }
        public string LastName { get => _lastName; set => SetProperty(ref _lastName, value); }
        public string Alias { get => _alias; set => SetProperty(ref _alias, value); }
    }
}
