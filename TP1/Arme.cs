﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    class Arme
    {
        private readonly string _name;
        private readonly int _minimalDamage;
        private readonly int _maximalDamage;
        private readonly ArmeType _weaponType;

        public string Nom { get => _name; }
        public int DegatsMinimaux { get => _minimalDamage; }
        public int DegatsMaximaux { get => _maximalDamage; }

        public ArmeType ArmeType { get => _weaponType; }

        public Arme(string name, int minimalDamage, int maximalDamage, ArmeType weaponType)
        {
            _name = name;
            _minimalDamage = minimalDamage;
            _maximalDamage = maximalDamage;
            _weaponType = weaponType;
        }

        public override string ToString() => $"{Nom} {{ {DegatsMinimaux}, {DegatsMaximaux}, {ArmeType} }}";
    }
}
