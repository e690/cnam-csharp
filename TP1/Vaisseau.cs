﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    class Vaisseau
    {
        private int _lifePoints;
        private int _currentLifePoints;
        private int _shieldPoints;
        private int _currentShieldPoints;
        private readonly List<Arme> _weapons;

        public int PointsDeVie
        {
            get => _lifePoints;
            set
            {
                if (_currentLifePoints != 0 && value >= 0 && value <= _lifePoints) _lifePoints = value;
            }
        }
        public int PointDeBouclier
        {
            get => _shieldPoints;
            set
            {
                if (_currentShieldPoints != 0 && value >= 0 && value <= _shieldPoints) _shieldPoints = value;
            }
        }
        public List<Arme> Armes { get => _weapons; }
        public bool EstDetruit { get => _shieldPoints == 0 && _lifePoints == 0; }

        public Vaisseau(int lifePoints, int shieldPoints = 0)
        {
            _lifePoints = lifePoints;
            _currentLifePoints = lifePoints;
            _shieldPoints = shieldPoints;
            _currentShieldPoints = shieldPoints;
            _weapons = new List<Arme>();
        }

        public void AjouterArme(Arme weapon)
        {
            if(_weapons.Count() == 3)
            {
                Console.WriteLine("Vous ne pouvez plus ajouter d'Arme à ce Vaisseau.");
                return;
            }
            _weapons.Add(weapon);
        }

        public void RetirerArme(Arme weapon)
        {
            if (!_weapons.Contains(weapon))
            {
                Console.WriteLine("Ce Vaisseau n'a pas cette Arme.");
                return;
            }
            _weapons.Remove(weapon);
        }

        public void MontrerArmes()
        {
            Console.WriteLine("Armes : \n");
            foreach(Arme weapon in _weapons)
            {
                Console.WriteLine(weapon.ToString());
            }
        }

        public double MoyenneDegats()
        {
            double res = 0;
            foreach(Arme weapon in _weapons)
            {
                res += (weapon.DegatsMinimaux + weapon.DegatsMaximaux) / 2;
            }
            return res;
        }

        public override string ToString() => $"Life Points: {PointsDeVie}\nShield Points: {PointDeBouclier}\nWeapons: \n{string.Join(",", Armes)}";
    }
}
