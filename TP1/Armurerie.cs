﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    class Armurerie
    {
        private List<Arme> _weaponList;

        public List<Arme> ListeArme { get => _weaponList; }

        public Armurerie()
        {
            Init();
        }

        private void Init()
        {
            _weaponList = new List<Arme>(
                new Arme[]{
                    new Arme("MachineGun", 10, 10, ArmeType.DIRECT),
                    new Arme("Grenade Launcher", 8, 15, ArmeType.EXPLOSIVE),
                    new Arme("Laser Beam", 1, 20, ArmeType.GUIDED)
                }
            );
        }

        public override string ToString()
        {
            string res =  "Armurerie : \n";
            foreach(Arme weapon in _weaponList)
            {
                res += $"{weapon}\n";
            }
            return res.Substring(0,res.Length - 1);
        }
    }
}
