﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TP1
{
    class SpaceInvaders
    {
        private Joueur[] _players;
        private Armurerie _armory;
        public SpaceInvaders()
        {
            Init();
        }

        public Armurerie Armurerie { get => _armory; }

        private void Init()
        {
            _armory = new Armurerie();
            _players = new Joueur[] { new Joueur("Yoann", "Bertuol", "Exdarface", new Vaisseau(200)), new Joueur("Ines", "Abbes", "Nessy", new Vaisseau(100, 100)), new Joueur("Victor", "Larmet", "Spartan", new Vaisseau(150, 50)) };
            for (int i = 0; i < _players.Length; i++)
            {
                _players[i].Vaisseau.AjouterArme(_armory.ListeArme[i]);
            }
        }

        static void Main(string[] args)
        {
            SpaceInvaders si = new SpaceInvaders();
            Console.WriteLine(si.Armurerie.ToString());
            foreach ( Joueur joueur in si._players)
            {
                Console.WriteLine(joueur.ToString());
                Thread.Sleep(2000);
            }
        }
    }
}
