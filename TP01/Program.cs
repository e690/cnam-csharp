﻿using System;
using System.Linq;
using System.Threading;

namespace TP01
{
    class Program
    {
        private static readonly String[] COMMENTARY = new String[] { "Careful Anorexy !", "You're a bit light", "You're perfect !", "Youre a bit overweight", "Medium overweight", "Important overweight", "Morbid Overweight" };
        private static readonly int MIN_HAIR = 100000;
        private static readonly int MAX_HAIR = 150000;

        static String DisplayFullName(String firstName, String lastName)
        {
            return $"{firstName} {lastName.ToUpper()}";
        }

        static Double ComputeIMC(int height, Double weight)
        {
            return weight / Math.Pow(Convert.ToDouble(height) / 100, 2);
        }

        static void GuessHair()
        {
            Boolean foundCorrectNumber = false;
            int userNumber = 0;

            Console.WriteLine("How much hair do you think you have?");
            while (foundCorrectNumber)
            {
                if(int.TryParse(Console.ReadLine(), out userNumber))
                {
                    if (Enumerable.Range(MIN_HAIR, MAX_HAIR).Contains(userNumber))
                    {
                        foundCorrectNumber = true;
                        Console.WriteLine("Decent");
                    } else 
                    {
                        Console.WriteLine("Be more specific");
                    }
                } else
                {
                    Console.WriteLine("Please Input a number");
                }

            }
        }
        static void CommentIMC(double imc)
        {
            // Switch Range (C# 7+ needed)
            switch(imc)
            {
                case double when imc < 16.5:
                    Console.WriteLine(COMMENTARY[0]);
                    break;
                case double when imc < 18.5:
                    Console.WriteLine(COMMENTARY[1]);
                    break;
                case double when imc < 25:
                    Console.WriteLine(COMMENTARY[2]);
                    break;
                case double when imc < 30:
                    Console.WriteLine(COMMENTARY[3]);
                    break;
                case double when imc < 35:
                    Console.WriteLine(COMMENTARY[4]);
                    break;
                case double when imc < 40:
                    Console.WriteLine(COMMENTARY[5]);
                    break;
                default:
                    Console.WriteLine(COMMENTARY[6]);
                    break;
            }
        }
        static void Run()
        {
            String firstName = String.Empty, lastName = String.Empty;
            int height = 0, oldness = 0, afterwork = 0;
            Double weight = 0;
            Console.WriteLine("Welcome, traveler.");
            while (true)
            {
                Console.WriteLine("What is your First Name ?");
                firstName = Console.ReadLine();
                if (!firstName.Any(char.IsDigit)) break;
                else Console.WriteLine("Please Input a First Name without number");
            }

            while (true)
            {
                Console.WriteLine("What is your Last Name ?");
                lastName = Console.ReadLine();
                if (!lastName.Any(char.IsDigit)) break;
                else Console.WriteLine("Please Input a Last Name without number");
            }

            // Using String Interpolation instead of Format (C# 6+ needed)
            Console.WriteLine($"Welcome {DisplayFullName(firstName, lastName)} \n");

            while (true)
            {
                Console.WriteLine("How tall are you? (cm)");
                if (int.TryParse(Console.ReadLine(), out height) && height > 0) break;
                else Console.WriteLine("Please Input a Height above zero");
            }

            while (true)
            {
                Console.WriteLine("How old are you?");
                if (int.TryParse(Console.ReadLine(), out oldness) && height > 0) break;
                else Console.WriteLine("Please Input a Height above zero");
            }

            while (true)
            {
                Console.WriteLine("How much do you weight? (kg)");
                if (Double.TryParse(Console.ReadLine(), out weight) && height > 0) break;
                else Console.WriteLine("Please Input a Height above zero");
            }

            if (oldness < 18)
            {
                Console.WriteLine("PH prohibited, get out.");
                Environment.Exit(0);
            }

            Double imc = ComputeIMC(height, weight);
            Console.WriteLine($"Your IMC is {imc:0.0}");
            CommentIMC(imc);

            GuessHair();

            while (true)
            {
                Console.WriteLine("Please choose: \n" +
                    "1 - Quit \n" +
                    "2 - Restart \n" +
                    "3 - Count to 10 \n" +
                    "4 - Ring Jacqueline");
                if (int.TryParse(Console.ReadLine(), out afterwork) && Enumerable.Range(1, 4).Contains(afterwork)) break;
                else Console.WriteLine("Please Input a displayed number");
            }

            switch (afterwork)
            {
                case 1:
                    Console.WriteLine("Goodbye");
                    Thread.Sleep(3000);
                    return;
                case 2:
                    Run();
                    break;
                case 3:
                    for (int i = 0; i < 10; i++)
                    {
                        Console.WriteLine(i);
                        Thread.Sleep(1000);
                    }
                    return;
                case 4:
                    Console.WriteLine("*crackling voice* I am missing due to New World launch at 8AM CEST, and won't be available for the next month, " +
                        "please leave a message and I will listen to it between maintenance and waiting queue");
                    break;
            }
        }
        static void Main()
        {
            Run();
        }
    }
}
