﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleGame
{
    class Menu
    {
        public static void Display()
        {
            int choice;

            Console.WriteLine("========== Menu SpaceInvaders ==========\n");
            Console.WriteLine("1. Gestion des joueurs");
            Console.WriteLine("2. Gestion du vaisseau");
            Console.WriteLine("3. Gestion de l'Armurerie");
            while (!int.TryParse(Console.ReadLine(), out choice) || !new[] { 1, 2, 3 }.Contains(choice))
            {
                Console.WriteLine("Nombre incorrect, réessayer");
            }
            switch (choice)
            {
                case 1:
                    MenuPlayers.Display();
                    break;
                case 2:
                    MenuShip.Display();
                    break;
                case 3:
                    break;
            }
        }
    }
}
