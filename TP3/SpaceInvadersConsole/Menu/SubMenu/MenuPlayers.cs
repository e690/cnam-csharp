﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleGame
{
    class MenuPlayers
    {
        public static void Display()
        {
            int choice;

            Console.WriteLine("========== Menu Joueurs ==========\n");
            Console.WriteLine("1. Créer un Joueur");
            Console.WriteLine("2. Supprimer un Joueur");
            Console.WriteLine("3. Retour");
            while (!int.TryParse(Console.ReadLine(), out choice) || !new[] { 1, 2, 3 }.Contains(choice))
            {
                Console.WriteLine("Nombre incorrect, réessayer");
            }
            switch (choice)
            {
                case 1:
                    CreatePlayer();
                    break;
                case 2:
                    DeletePlayer();
                    break;
                case 3:
                    Menu.Display();
                    break;
            }
        }

        public static void CreatePlayer()
        {
            string firstName, lastName, pseudo;
            Console.WriteLine("Prénom :");
            firstName = Console.ReadLine();
            Console.WriteLine("Nom :");
            lastName = Console.ReadLine();
            Console.WriteLine("Pseudo :");
            pseudo = Console.ReadLine();
            SpaceInvadersConsole.Instance.Players.Add(new Models.Player(firstName, lastName, pseudo));
            Display();
        }

        public static void DeletePlayer()
        {
            List<Models.Player> players = SpaceInvadersConsole.Instance.Players;
            int choice;
            for (int i = 0; i < players.Count(); i++)
            {
                Console.WriteLine($"{i + 1}. {players[i]}");
            }
            Console.WriteLine("Choisis quel joueur supprimer");
            while (!int.TryParse(Console.ReadLine(), out choice) || (choice >= 1 && choice < players.Count()))
            {
                Console.WriteLine("Nombre incorrect, réessayer");
            }
            players.RemoveAt(choice - 1);
            Display();
        }
    }
}
