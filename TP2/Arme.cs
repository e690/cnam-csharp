﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Arme
    {
        private readonly string _name;
        private readonly int _minimalDamage;
        private readonly int _maximalDamage;
        private readonly double _timeReload;
        private readonly ArmeType _weaponType;

        private double _counterReload;

        public string Nom { get => _name; }
        public int DegatsMinimaux { get => _minimalDamage; }
        public int DegatsMaximaux { get => _maximalDamage; }

        public bool EstUtilisable { get => _counterReload - _timeReload == 0; }

        public ArmeType ArmeType { get => _weaponType; }

        public Arme(string name, int minimalDamage, int maximalDamage, double timeReload, ArmeType weaponType)
        {
            _name = name;
            _minimalDamage = minimalDamage;
            _maximalDamage = maximalDamage;
            _timeReload = timeReload;
            _counterReload = timeReload;
            _weaponType = weaponType;
        }

        public int Tirer()
        {
            _counterReload--;
            if(_counterReload <= 0)
            {
                int damage = 0;
                Random randomizer = new Random();
                switch (_weaponType)
                {
                    case ArmeType.DIRECT:
                        if (randomizer.Next(10) != 0)
                        {
                            damage = randomizer.Next(_minimalDamage, _maximalDamage + 1);
                            _counterReload = _timeReload;
                        }
                        break;
                    case ArmeType.EXPLOSIVE:
                        if (randomizer.Next(4) != 0) {
                            damage = randomizer.Next(_minimalDamage, _maximalDamage + 1) * 2;
                            _counterReload = _timeReload * 2;
                        }
                        break;
                    case ArmeType.GUIDED:
                        damage = _minimalDamage;
                        _counterReload = _timeReload;
                        break;
                }
                return damage;
            }
            return 0;
        }

        public override string ToString() => $"{Nom} {{ {DegatsMinimaux}, {DegatsMaximaux}, {ArmeType} }}";
    }
}
