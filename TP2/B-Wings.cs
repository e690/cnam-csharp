﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class B_Wings : Vaisseau
    {
        public B_Wings(Armurerie armurerie) : base(30)
        {
            AjouterArme(armurerie.ListeArme.Find(weapon => weapon.Nom == "Hammer"));
        }
        public override void Attaque(Vaisseau ship)
        {
            if (!EstDetruit)
            {
                int damage = 0;
                foreach (Arme weapon in Armes) if (weapon.ArmeType == ArmeType.EXPLOSIVE) damage += weapon.Tirer();
                ship.SubisDegats(damage);
            }
        }
    }
}
