﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Dart : Vaisseau
    {
        public Dart(Armurerie armurerie) : base(10, 3)
        {
            AjouterArme(armurerie.ListeArme.Find(weapon => weapon.Nom == "Laser"));
        }
        public override void Attaque(Vaisseau ship)
        {
            if (!EstDetruit)
            {
                int damage = 0;
                foreach (Arme weapon in Armes) if (weapon.ArmeType == ArmeType.DIRECT) damage += weapon.Tirer();
                ship.SubisDegats(damage);
            }
        }
    }
}
