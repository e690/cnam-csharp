﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class F_18 : Vaisseau, IAptitude
    {
        public F_18() : base(15)
        {

        }

        public override void Attaque(Vaisseau ship)
        {
        }

        public void Utilise(List<Vaisseau> ships)
        {
            if (!EstDetruit)
            {
                for (int i = 1; i < ships.Count(); i++)
                {
                    if (ships[i] == this) ships[i - 1].SubisDegats(10);
                }
                this.PointsDeVie = 0;
            }
        }
    }
}
