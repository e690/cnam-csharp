﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Armurerie
    {
        private List<Arme> _weaponList;

        public List<Arme> ListeArme { get => _weaponList; }

        public Armurerie()
        {
            Init();
        }

        private void Init()
        {
            _weaponList = new List<Arme>(
                new Arme[]{
                    new Arme("Laser", 2, 3, 1, ArmeType.DIRECT),
                    new Arme("Hammer", 1, 8, 1.5, ArmeType.EXPLOSIVE),
                    new Arme("Torpille", 3, 3, 2, ArmeType.GUIDED),
                    new Arme("Mitrailleuse", 2, 3, 1, ArmeType.DIRECT),
                    new Arme("EMG", 1, 7, 1.5, ArmeType.EXPLOSIVE),
                    new Arme("Missile", 4, 100, 4, ArmeType.GUIDED)
                }
            );
        }

        public override string ToString()
        {
            string res =  "Armurerie : \n";
            foreach(Arme weapon in _weaponList)
            {
                res += $"{weapon}\n";
            }
            return res.Substring(0,res.Length - 1);
        }
    }
}
