﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    interface IAptitude
    {
        void Utilise(List<Vaisseau> ships);
    }
}
