﻿
namespace TP2
{
    class Joueur
    {
        private readonly string _firstName;
        private readonly string _lastName;
        private readonly string _pseudo;
        private readonly Vaisseau _ship;

        public string NomComplet { get => string.Join(" ", new string[]{ _firstName, _lastName }); }
        public string Pseudo { get => _pseudo; }
        public Vaisseau Vaisseau { get => _ship; }
        public Joueur(string firstName, string lastName, string pseudo, Vaisseau ship)
        {
            string[] tempArrayNames = FormatNames(firstName, lastName);
            _firstName = tempArrayNames[0];
            _lastName = tempArrayNames[1];
            _pseudo = pseudo;
            _ship = ship;
        }
        private static string[] FormatNames(string firstName, string lastName) => new string[] { firstName[0].ToString().ToUpper() + firstName.Substring(1).ToLower(), lastName[0].ToString().ToUpper() + lastName.Substring(1).ToLower() };
        public override string ToString() => $"{Pseudo} ( {NomComplet} ) \nShip: \n{Vaisseau}";

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Joueur)) return false;
            return this.Pseudo.Equals(((Joueur) obj).Pseudo);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
