﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    abstract class Vaisseau
    {
        private readonly int _lifePoints;
        private int _currentLifePoints;
        private readonly int _shieldPoints;
        private int _currentShieldPoints;
        private readonly List<Arme> _weapons;

        public int PointsDeVie
        {
            get => _currentLifePoints;
            set
            {
                if (_currentLifePoints != 0 && value >= 0) _currentLifePoints = _currentLifePoints + value >= _lifePoints ? _lifePoints : _currentLifePoints + value;
            }
        }
        public int PointDeBouclier
        {
            get => _currentShieldPoints;
            set
            {
                if (_currentShieldPoints != 0 && value >= 0) _currentShieldPoints = _currentShieldPoints + value >= _shieldPoints ? _shieldPoints : _currentShieldPoints + value;
            }
        }
        public int PointDeBouclierMax { get => _shieldPoints; }
        public List<Arme> Armes { get => _weapons; }
        public bool EstDetruit { get => _shieldPoints == 0 && _lifePoints == 0; }

        public Vaisseau(int lifePoints, int shieldPoints = 0)
        {
            _lifePoints = lifePoints;
            _currentLifePoints = lifePoints;
            _shieldPoints = shieldPoints;
            _currentShieldPoints = shieldPoints;
            _weapons = new List<Arme>();
        }

        public void AjouterArme(Arme weapon)
        {
            if(_weapons.Count() == 3)
            {
                Console.WriteLine("Vous ne pouvez plus ajouter d'Arme à ce Vaisseau.");
                return;
            }
            _weapons.Add(weapon);
        }

        public void RetirerArme(Arme weapon)
        {
            if (!_weapons.Contains(weapon))
            {
                Console.WriteLine("Ce Vaisseau n'a pas cette Arme.");
                return;
            }
            _weapons.Remove(weapon);
        }

        public void MontrerArmes()
        {
            Console.WriteLine("Armes : \n");
            foreach(Arme weapon in _weapons)
            {
                Console.WriteLine(weapon.ToString());
            }
        }

        public double MoyenneDegats()
        {
            double res = 0;
            foreach(Arme weapon in _weapons)
            {
                res += (weapon.DegatsMinimaux + weapon.DegatsMaximaux) / 2;
            }
            return res;
        }

        public void SubisDegats(int damageNumber)
        {
            if(_currentShieldPoints != 0)
            {
                if(_currentShieldPoints >= damageNumber)
                {
                    _currentShieldPoints -= damageNumber;
                } else
                {
                    damageNumber -= _currentShieldPoints;
                    _currentShieldPoints = 0;
                }
            }
            if(_currentLifePoints != 0)
            {
                if (_currentLifePoints >= damageNumber)
                {
                    _currentLifePoints -= damageNumber;
                } else
                {
                    _currentLifePoints = 0;
                }
            }
        }

        public abstract void Attaque(Vaisseau ship);
        public override string ToString() => $"Life Points: {PointsDeVie}/{_lifePoints}\nShield Points: {PointDeBouclier}/{_shieldPoints}\nWeapons: \n{string.Join(",", Armes)}";
    }
}
