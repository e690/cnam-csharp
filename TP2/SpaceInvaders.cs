﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TP2
{
    class SpaceInvaders
    {
        private List<Joueur> _players;
        private Armurerie _armory;
        private List<Vaisseau> _listEnemies;
        public SpaceInvaders()
        {
            Init();
        }

        public Armurerie Armurerie { get => _armory; }

        private void Init()
        {
            _armory = new Armurerie();
            _players = new List<Joueur> { new Joueur("Yoann", "Bertuol", "Exdarface", new Dart(_armory)), new Joueur("Ines", "Abbes", "Nessy", new Rocinante(_armory)), new Joueur("Victor", "Larmet", "Spartan", new B_Wings(_armory)) };
            _listEnemies = new List<Vaisseau>() { new Dart(_armory), new B_Wings(_armory), new Tardis(), new F_18(), new Rocinante(_armory), new ViperMKII(_armory)};
        }
        private void JouerUnTour()
        {
            Random randomizer = new Random();
            List<Vaisseau> fullList = new List<Vaisseau>(_listEnemies);
            List<Vaisseau> copyEnemies = new List<Vaisseau>(_listEnemies);
            foreach (Joueur player in _players) for (int i = 0; i < _listEnemies.Count; i++) if (randomizer.Next(i, _listEnemies.Count) == _listEnemies.Count) fullList.Insert(randomizer.Next(i), player.Vaisseau);

            foreach (Vaisseau ship in _listEnemies) if (ship is IAptitude aptitude)
                    aptitude.Utilise(copyEnemies);
            _listEnemies = copyEnemies;
            foreach (Vaisseau ship in fullList)
            {
                bool playerShip = false;
                foreach (Joueur player in _players) if (player.Vaisseau == ship) playerShip = true;
                if(playerShip)
                {
                    List<Vaisseau> notDestroyedShips = _listEnemies.FindAll(s => !s.EstDetruit);
                    ship.Attaque(notDestroyedShips[randomizer.Next(0, notDestroyedShips.Count)]);
                } else
                {
                    ship.Attaque(_players[randomizer.Next(0, _players.Count)].Vaisseau);
                }
            }
            foreach (Vaisseau ship in fullList) if (ship.PointDeBouclier <= ship.PointDeBouclierMax) ship.PointDeBouclier += 2;
        }
        static void Main(string[] args)
        {
            SpaceInvaders si = new SpaceInvaders();
            bool playersCanPlay = false, enemiesExists = false;
            do
            {
                foreach (Joueur player in si._players)
                    if (!player.Vaisseau.EstDetruit)
                    {
                        playersCanPlay = true;
                        break;
                    }

                foreach (Vaisseau ship in si._listEnemies)
                    if (!ship.EstDetruit)
                    {
                        enemiesExists = true;
                        break;
                    }
                si.JouerUnTour();
            } while (playersCanPlay && enemiesExists);
            if (playersCanPlay) Console.WriteLine("players won");
            else Console.WriteLine("enemies won");
            
        }
    }
}
