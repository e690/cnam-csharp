﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Tardis : Vaisseau, IAptitude 
    {
        public Tardis() : base(1)
        {

        }

        public override void Attaque(Vaisseau ship)
        {
        }

        public void Utilise(List<Vaisseau> ships)
        {
            if(!EstDetruit)
            {
                Random randomizer = new Random();
                int firstIndex = randomizer.Next(0, ships.Count);
                int secondIndex = randomizer.Next(0, ships.Count);

                Vaisseau tempShip = ships[firstIndex];
                ships[firstIndex] = ships[secondIndex];
                ships[secondIndex] = tempShip;
            }
        }
    }
}
