﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Rocinante : Vaisseau
    {
        public Rocinante(Armurerie armurerie) : base(3, 5)
        {
            AjouterArme(armurerie.ListeArme.Find(weapon => weapon.Nom == "Torpille"));
        }
        public override void Attaque(Vaisseau ship)
        {
            if (!EstDetruit)
            {
                int damage = 0;
                foreach (Arme weapon in Armes)
                    damage += weapon.Tirer();
                ship.SubisDegats(damage);
            }
            
        }
    }
}
