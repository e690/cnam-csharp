﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class ViperMKII : Vaisseau
    {
        public ViperMKII(Armurerie armurerie) : base(10, 15)
        {
            AjouterArme(armurerie.ListeArme.Find(weapon => weapon.Nom == "Mitrailleuse"));
            AjouterArme(armurerie.ListeArme.Find(weapon => weapon.Nom == "EMG"));
            AjouterArme(armurerie.ListeArme.Find(weapon => weapon.Nom == "Missile"));
        }
        public override void Attaque(Vaisseau ship)
        {
            if(!EstDetruit)
            {
                int damage = 0;
                foreach (Arme weapon in Armes)
                    damage += weapon.Tirer();
                ship.SubisDegats(damage);
            }
        }
    }
}
